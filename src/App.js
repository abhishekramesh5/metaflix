import React from 'react';
import Navbar from './components/Navbar';
import './App.css';
import Home from './components/pages/Home';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Services from './components/pages/Services';
import PricingNav from './components/pages/PricingNav';
import Buy from './components/pages/SignUp';

function App() {
  return (
    <>
      <Router>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path='/services' element={<Services />} />
          <Route path='/pricing' element={<PricingNav />} />
          <Route path='/Buy' element={<Buy />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
