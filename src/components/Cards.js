import React from 'react';
import './Cards.css';
import CardItem from './CardItem';

function Cards() {
  return (
    <div className='cards'>
      <h1>Tokenomics</h1>
      <div className='cards__container'>
        <div className='cards__wrapper'>
          <ul className='cards__items'>
            <CardItem
              src='images/img-9.jpg'
              text='Manual Burn'
              label='Burn'
              path='/products'
            />
            <CardItem
              src='images/img-2.jpg'
              text='5% Redistributed To Holders'
              label='Holders Redistribution'
              path='/products'
            />
          </ul>
          <ul className='cards__items'>
            <CardItem
              src='images/img-3.jpg'
              text='3% To Charity Wallet'
              label='Charity'
              path='/products'
            />
            <CardItem
              src='images/img-4.jpg'
              text='4% Added To The Liquidity Pool'
              label='Liquidity'
              path='/products'
            />
            <CardItem
              src='images/img-8.jpg'
              text='Blockchain Products For Our Holders'
              label='Blockchain products'
              path='/sign-up'
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Cards;
