import React from 'react';
import '../../App.css';
import Pricing from '../Pricing'; 
import Footer from '../Footer';

export default function PricingNav() {
  return (
    <>
      <Pricing />
      <Footer />     
    </>
  );
}
