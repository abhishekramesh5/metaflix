import React from 'react';
import FaqsContainer from '../FaqsContainer';
import Footer from '../Footer';

import '../../App.css';

export default function Services() {
  return (
    <>
      <FaqsContainer />  
      <Footer />
    </>
  );
}
